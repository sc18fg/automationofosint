#pylint: disable=C0301
import json
import requests
import globalvar as gv

# Get relevant API key from apikeys.json
with open('apikeys.json') as f:
    key = json.load(f)
    VIRUSTOTAL_API_KEY = (key["apikeys"][0]["virustotal"])
    f.close()

def title_format():
    gv.MALREPORT.write("\n__________________________________________\n")
    gv.MALREPORT.write("\n\t\tVirusTotal")
    gv.MALREPORT.write("\n__________________________________________\n")

# When an IP address is inputted by user
def vt_ip(scan_input):

    # Declare variables & lists to manipulate information from API
    antivirus_solution = 0
    number_of_detections = 0
    result_engine = []
    name_of_engine = []

    # GET information from website
    response = requests.get("https://www.virustotal.com/api/v3/ip_addresses/%s" % scan_input, headers={'User-agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0', 'x-apikey': '%s' % VIRUSTOTAL_API_KEY})
    response_status = response.status_code
    response = response.json()

    if response_status == 401:
        title_format()
        gv.MALREPORT.write("\n\nAPI key authentication failed.")
    elif response_status == 404:
        title_format()
        gv.MALREPORT.write("\n\nThe requested resource failed or was not found.")
    elif response_status == 400:
        title_format()
        gv.MALREPORT.write("\n\nUnable to scan this IP address.")

    elif response_status == 403:
        title_format()
        gv.MALREPORT.write("\n\nNot allowed to perform the requested operation.")

    elif response_status == 424:
        title_format()
        gv.MALREPORT.write("\n\nThe requested resource failed or was not found.")

    elif response_status == 429:
        title_format()
        gv.MALREPORT.write("\n\nYou have exceeded the rate-limit for scans. Please try again later.")

    elif response_status == 500:
        title_format()
        gv.MALREPORT.write("\n\nError with VirusTotal server. Please try again later.")

    elif response_status == 503:
        title_format()
        gv.MALREPORT.write("\n\nError with VirusTotal server. Please try again later.")

    elif response_status == 504:
        title_format()
        gv.MALREPORT.write("\n\nOperation took too long to complete.")

    else:
        try:
            # Extract relevant information from JSON response
            data = response["data"]["attributes"]["last_analysis_results"]
            if response_status == 200:
                for i in data:
                    # Check how many and which antivirus solutions
                    # believe the input is malicious/suspicious
                    if data[i]["category"] == "suspicious" or data[i]["category"] == "malicious":
                        # Add the antivirus solution name to a list to
                        # display that for user in text file
                        name_of_engine.append(data[i]["engine_name"])
                        result_engine.append(data[i]["result"])
                        number_of_detections = number_of_detections + 1

                    antivirus_solution = antivirus_solution + 1

                # If there is 1 or more antivirus solution deeming the input
                # as malicious/suspicious - display this for user in text file
                if number_of_detections > 0:

                    engine = str(name_of_engine)[1:-1]
                    detect = str(number_of_detections)
                    num = str(antivirus_solution)

                    # Output information to text file
                    title_format()
                    mal_result = f"\nIP Address: {scan_input}\n\nMarked malicious or suspiscious by {detect}/{num} engines: \n{engine}."
                    gv.MALREPORT.write("\n" + mal_result)
                else:
                    title_format()
                    res = f"\nThis IP Address has been marked clean.\n"
                    gv.MALREPORT.write("\n" + res)

        except KeyError:
            title_format()
            gv.MALREPORT.write("\n\nNo information is available for this IP Address.")

# When a URL is inputted by user
def vt_url(scan_input):

    # Declare variables & lists to manipulate information from API
    antivirus_solution = 0
    number_of_detections = 0
    result_engine = []
    name_of_engine = []

    # Request information from website
    response = requests.get("https://www.virustotal.com/api/v3/domains/%s" % scan_input, headers={'User-agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0', 'x-apikey': '%s' % VIRUSTOTAL_API_KEY})
    response_status = response.status_code
    response = response.json()

    if response_status == 401:
        title_format()
        gv.MALREPORT.write("\n\nAPI key authentication failed.")
    elif response_status == 404:
        title_format()
        gv.MALREPORT.write("\n\nThe requested resource failed or was not found.")
    elif response_status == 400:
        title_format()
        gv.MALREPORT.write("\n\nUnable to scan this IP address.")
    elif response_status == 403:
        title_format()
        gv.MALREPORT.write("\n\nNot allowed to perform the requested operation.")

    elif response_status == 424:
        title_format()
        gv.MALREPORT.write("\n\nThe requested resource failed or was not found.")

    elif response_status == 429:
        title_format()
        gv.MALREPORT.write("\n\nYou have exceeded the rate-limit for scans. Please try again later.")

    elif response_status == 500:
        title_format()
        gv.MALREPORT.write("\n\nError with VirusTotal server. Please try again later.")

    elif response_status == 503:
        title_format()
        gv.MALREPORT.write("\n\nError with VirusTotal server. Please try again later.")

    elif response_status == 504:
        title_format()
        gv.MALREPORT.write("\n\nOperation took too long to complete.")

    else:
        try:
        # Extract relevant information from JSON file
            data = response["data"]["attributes"]["last_analysis_results"]

            if response_status == 200:
                for i in data:

                    antivirus_solution = antivirus_solution + 1

                    # Check how many and which antivirus solutions
                    # believe the input is malicious/suspicious
                    if data[i]["category"] == "suspicious" or data[i]["category"] == "malicious":
                        # Add the antivirus solution name to a list
                        # to display that for user in text file
                        result_engine.append(data[i]["result"])
                        name_of_engine.append(data[i]["engine_name"])
                        number_of_detections = number_of_detections + 1

                # If there is 1 or more antivirus solution deeming the input
                # as malicious/suspicious - display this for user in text file
                if number_of_detections > 0:

                    engine = str(name_of_engine)[1:-1]
                    detect = str(number_of_detections)
                    num = str(antivirus_solution)

                    # Output information to text file
                    title_format()
                    mal_result = f"\nURL: {scan_input}\n\nMarked malicious or suspiscious by {detect}/{num} engines: \n{engine}."
                    gv.MALREPORT.write("\n" + mal_result)
                else:
                    title_format()
                    res = "\nThis URL/root domain has been marked clean."
                    gv.MALREPORT.write("\n" + res)

        except KeyError:
            title_format()
            gv.MALREPORT.write("\n\nNo information is available for this URL.")
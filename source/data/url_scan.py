#pylint: disable=C0301
import json
import time
import urllib.request
import requests
import globalvar as gv
from PIL import Image

# Get relevant API key from apikeys.json file
with open('apikeys.json') as f:
    key = json.load(f)
    URLSCAN_API_KEY = (key["apikeys"][1]["urlscan"])
    f.close()

def title_format():
    gv.MALREPORT.write("\n__________________________________________\n")
    gv.MALREPORT.write("\n\t\turlscan.io")
    gv.MALREPORT.write("\n__________________________________________\n")
    
def url_scan(scan_input):
    try:
        # For POST
        header = {'API-Key': '%s' % URLSCAN_API_KEY,'Content-Type':'application/json'}
        data = {"url": "%s" % scan_input, "visibility": "public"}
        # POST to website to get UUID for scanning
        request = requests.post("https://urlscan.io/api/v1/scan/", headers=header, data=json.dumps(data))
        post_status = request.status_code

        if post_status == 401:
            gv.MALREPORT.write("\nurlscan.io")
            gv.MALREPORT.write("\n\n\nAPI key authentication failed.")

        else:
            request_json = request.json()     
            # UUID required to GET results
            uuid = request_json['uuid']
            # API requires at least 10 seconds to scan URL
            time.sleep(15)
            # For GET
            header = {'API-key': '%s' % URLSCAN_API_KEY}
            # GET scan results from website
            request_get = requests.get("https://urlscan.io/api/v1/result/%s/" % uuid, headers=header)
            get_status = request_get.status_code
            r_json = request_get.json()
            # Meaningful error messages based on HTTP status codes
            if (post_status and get_status == 200):
                # Extract relevant information from JSON response
                url = r_json['page']['url']
                score = r_json['verdicts']['overall']['score']
                cat = r_json['verdicts']['overall']['categories']
                tags = r_json['verdicts']['overall']['tags']
                mal = r_json['verdicts']['overall']['malicious']

                # Output information to text file
                title_format()
                res = f"URL: {url}\n\nCategory: {cat}.\n\nTags: {tags}.\n\nIs the URL malicious?: {mal}.\n\nThe URL has a malicious score of {score}/100."
                gv.MALREPORT.write("\n" + res)          
                # Get PNG image (screenshot of website)
                get_image = "%s.png" % uuid
                urllib.request.urlretrieve("https://urlscan.io/screenshots/%s.png" % uuid, get_image)
                image = Image.open(get_image)
                image.show()
            elif get_status == 400:
                title_format()
                gv.MALREPORT.write("\n\nDNS Error - could not resolve domain to a valid IPv4/IPv6 address.")

            elif get_status == 429:
                title_format()
                gv.MALREPORT.write("\n\nYou have exceeded the rate-limit for scans. Please try again later.")

            elif get_status == 404:
                title_format()
                gv.MALREPORT.write("\n\nScan did not finish in time - please try again.")

            elif get_status == 500:
                title_format()
                gv.MALREPORT.write("\n\nIssue with urlscan.io's servers - please try again later.")

            else:
                title_format()
                gv.MALREPORT.write("\n\nHas no information is available for this URL/root domain.")
    except KeyError:
        title_format()
        gv.MALREPORT.write("\n\nurlscan.io cannot scan this URL/root domain.")
   
#pylint: disable=C0301
import requests
import globalvar as gv

# No API key required for IPWHOIS.io

def title_format():
    gv.MALREPORT.write("\n__________________________________________\n")
    gv.MALREPORT.write("\n\t\tipwhois.io")
    gv.MALREPORT.write("\n__________________________________________\n")

# When an IP address is inputted by user
def who_is(scan_input):

    try:
         # GET information from website
        response = requests.get(f"http://ipwhois.app/json/{scan_input}?objects=ip,country,continent_code,city,org,isp,region,timezone")
        response_status = response.status_code
        response = response.json()

        if response_status == 200:

            # Extract relevant information from JSON response
            ip_add = response['ip']
            cont = response['continent_code']
            country = response['country']
            city = response['city']
            org = response['org']
            isp = response['isp']
            region = response['region']
            timezone = response['timezone']

            # Output information to text file
            title_format()
            result = f"\nIP Address: {ip_add} \n\nLocation: {country}, {city}, {region}\n\nContinent code: {cont}\n\nOwned by: {org}\n\nInternet Service Provider: {isp}\n\nCity timezone: {timezone}"
            gv.MALREPORT.write("\n" + result + "\n")      
        
        elif response_status == 400:
            title_format()
            gv.MALREPORT.write("\n\nUnable to scan this IP address.")
        else:
            title_format()
            gv.MALREPORT.write("\n\n\n\tHas no information is available for this IP address.")

    except KeyError:
        title_format()
        gv.MALREPORT.write("\n\n\n\tNo information is available for this IP address.")

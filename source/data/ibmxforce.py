#pylint: disable=C0301
import json
import requests
from requests.auth import HTTPBasicAuth
import globalvar as gv

# Get relevant API key from apikeys.json file
with open('apikeys.json') as f:
    key = json.load(f)
    API_KEY = (key["apikeys"][2]["ibmxforce_key"])
    # IBM X-Force Exchange API requires user to have a password as well as a key in order to access information
    API_PASSWORD = (key["apikeys"][3]["ibmxforce_pass"])
    f.close()

auth=HTTPBasicAuth(API_KEY, API_PASSWORD)
xfex_cred = f"{API_KEY}:{API_PASSWORD}"

headers = {"Authorization": "Basic %s " % xfex_cred, "Accept": "application/json"}

def title_format():
    gv.MALREPORT.write("\n__________________________________________\n")
    gv.MALREPORT.write("\n\t\tIBM X-Force Exchange")
    gv.MALREPORT.write("\n__________________________________________\n")


# Function for when an IP address is inputted by user
def ibm_ip(scan_input):

    # Request IP reputation information from website
    response = requests.get("https://exchange.xforce.ibmcloud.com/api/ipr/%s" % scan_input, auth=HTTPBasicAuth(API_KEY, API_PASSWORD))
    response_status = response.status_code
    response = response.json()
    # Request DNS resolution if possible
    response_resolution = requests.get("https://exchange.xforce.ibmcloud.com/api/resolve/%s" % scan_input, auth=HTTPBasicAuth(API_KEY, API_PASSWORD))
    rr_status = response_resolution.status_code
    response_resolution = response_resolution.json()
    # Request malware information
    response_malware = requests.get("https://exchange.xforce.ibmcloud.com/api/ipr/malware/%s" % scan_input, auth=HTTPBasicAuth(API_KEY, API_PASSWORD))
    rm_status = response_malware.status_code
    response_malware = response_malware.json()
    # Meaningful error messages based on HTTP status codes

    if response_status == 400:
        title_format()
        gv.MALREPORT.write("\n\nAn error has resulted due to the IP address input.")

    elif response_status == 429:
        title_format()
        gv.MALREPORT.write("\n\nYou have exceeded the rate-limit for scans. Please try again later.")

    elif response_status == 500:
        title_format()
        gv.MALREPORT.write("\n\nThere is an error with IBM X-Force Exchange's servers.")

    elif response_status == 401:
        title_format()
        gv.MALREPORT.write("\n\nAPI key/password authentication failed.")

    elif response_status == 200:
        try:

            # Extract relevant information from JSON response
            score = response['score']
            reason = response['reason']
            geo = response['geo']['country']
            geo2 = response['geo']['countrycode']

            title_format()
            result = f"IP Address: {scan_input} \n\nRisk score: {score}/10\n\nReason: {reason}\n\nLocation: {geo}, {geo2}"
            gv.MALREPORT.write("\n" + result + "\n\n")
            if rr_status == 200:

                if "RDNS" in response_resolution:
                    domain_resolution = response_resolution['RDNS']
                    r_dns = f"DNS: {domain_resolution}\n\n"
                    gv.MALREPORT.write(r_dns)

            if rm_status == 200:
                try:
                    malware = response_malware['malware'][0]['family']
                    malware_result = f"Malware: {malware}\n"
                    gv.MALREPORT.write(malware_result)
                except KeyError:
                    return

        except KeyError:
            title_format()
            gv.MALREPORT.write("\n\nNo information for this IP address.")
    else:
        title_format()
        gv.MALREPORT.write("\n\n\nNo information for this URL.")

# Function for when a URL is inputted by user
def ibm_url(scan_input):

    # Request URL report information from website
    response = requests.get("https://exchange.xforce.ibmcloud.com/api/url/%s/" % scan_input, auth=HTTPBasicAuth(API_KEY, API_PASSWORD))
    response_status = response.status_code
    response = response.json()

    # Request IP address resolution
    response_resolution = requests.get("https://exchange.xforce.ibmcloud.com/api/resolve/%s" % scan_input, auth=HTTPBasicAuth(API_KEY, API_PASSWORD))
    rr_status = response_resolution.status_code
    response_resolution = response_resolution.json()

    # Request contact email address
    response_whois = requests.get("https://exchange.xforce.ibmcloud.com/api/whois/%s" % scan_input, auth=HTTPBasicAuth(API_KEY, API_PASSWORD))
    rw_status = response_whois.status_code
    response_whois = response_whois.json()

    if response_status == 400:
        title_format()
        gv.MALREPORT.write("\n\nAn error has resulted due to the URL/root domain input.")
    elif response_status == 429:
        title_format()
        gv.MALREPORT.write("\n\nYou have exceeded the rate-limit for scans. Please try again later.")
    elif response_status == 500:
        title_format()
        gv.MALREPORT.write("\n\nThere is an error with IBM X-Force Exchange's servers.")

    elif response_status == 401:
        title_format()
        gv.MALREPORT.write("\n\n\nAPI key/password authentication failed.")
    elif response_status == 200:
        try:
            # Extract relevant information from JSON response
            score = response['result']['score']
            cat = response['result']['cats']
            cat_descrip = response['result']['categoryDescriptions']

            title_format()
            result = f"URL: {scan_input} has the risk score: {score}/10.\n\nCategory: {cat}\n\nDescription: {cat_descrip}"
            gv.MALREPORT.write("\n" + result + "\n\n")

            if rr_status == 200:
                if 'A' in response_resolution:
                    ip_resolution = response_resolution['A']
                    r_dns = f"IP: {ip_resolution}\n\n"
                    gv.MALREPORT.write(r_dns)
            if rw_status == 200:
                if 'contactEmail' in response_whois:
                    email = response_whois['contactEmail']
                    email_result = f"Contact email: {email}\n\n"
                    gv.MALREPORT.write(email_result)

        except KeyError:
            title_format()
            gv.MALREPORT.write("\n\nNo information for this URL.")
    else:
        title_format()
        gv.MALREPORT.write("\n\n\nNo information for this URL.")
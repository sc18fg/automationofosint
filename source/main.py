#pylint: disable=C0301
import sys
import ipaddress

from PyQt5.QtWidgets import QMainWindow, QLabel, QLineEdit, QPushButton
from PyQt5.QtCore import QSize, Qt
from PyQt5.QtGui import QPalette
from PyQt5.QtWebEngineWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import QMessageBox

from data.whois import who_is
from data.url_scan import url_scan
from data.vt import vt_url, vt_ip
from data.ibmxforce import ibm_ip, ibm_url
import globalvar

class MainWindow(QMainWindow):
    def __init__(self):
        
        super().__init__()
        QMainWindow.__init__(self)
        app.setStyle("Fusion")

        qp = QPalette()
        qp.setColor(QPalette.ButtonText, Qt.black)
        qp.setColor(QPalette.Window, Qt.white)
        qp.setColor(QPalette.Button, Qt.gray)
        app.setPalette(qp)

        # Size of main input window
        self.setMinimumSize(QSize(450, 160))
        self.setWindowTitle("Automation of OSINT")

        # Label for labelling what type of IOC input
        self.nameLabel = QLabel(self)
        self.nameLabel.setText("Enter an IPv4 address,\nURL or root domain:")
        self.nameLabel.resize(215, 60)
        self.nameLabel.move(20, 20)
        
        # Text box to input IOC
        self.line = QLineEdit(self)
        self.line.resize(200, 40)
        self.line.move(210, 30)

        # Warning for time taken to scan some IOCs
        self.loadLabel = QLabel(self)
        self.loadLabel.setText("Some scans may take 10 seconds to load")
        self.loadLabel.resize(340, 50)
        self.loadLabel.move(75, 110)

        # Button to submit search
        submitButton = QPushButton("Submit", self)
        submitButton.clicked.connect(self.info_output)
        submitButton.resize(210, 30)
        submitButton.move(120, 90)  

    def info_output(self):

        # Using entered IOC, call API request functions for each OSINT resource
        scan_input = self.line.text()
        output = main(scan_input)
        
        # If an exception is thrown, show error window
        # If not, retrieve OSINT information 
        if output == 0:
            self.dialog = ErrorWindow()
        else:
            self.dialog = ShowDialog()
    
class ShowDialog():

    def __init__(self):

        super().__init__()

        # Size of OSINT information output window
        self.browser = QTextBrowser()
        self.browser.setMinimumSize(QSize(500, 730))    
        self.browser.setWindowTitle("Automation of OSINT")

        # Load text from text file, output.txt, of retrieved OSINT information
        # Output text onto window
        self.data = QTextEdit()
        recentScan = open("output.txt", "r+")
        self.data = recentScan.read()
        self.browser.setText(self.data)

        # Create a log file, log.txt, to log all requests
        logFile = open("log.txt", "a+")
        
        # Write OSINT information retrieved to log.txt from output.txt
        with open("output.txt", "r") as scan:
            logFile.write(scan.read())
            logFile.close()
        
        # Clear output.txt file so it contains only the most recent scan
        recentScan.truncate(0)
                
        output = self.browser
        output.show()

class ErrorWindow():
    
    def __init__(self):
        super().__init__()

        # Error message window
        self.error = QMessageBox()
        self.error.setIcon(QMessageBox.Critical)
        self.error.setText("ERROR")
        self.error.setInformativeText('Invalid IOC entered - please try again.')
        self.error.setWindowTitle("Error")
        self.error.exec_() 

def main(scan_input):

    globalvar.MALREPORT = open('output.txt','a')
    
    try:
        # Scan URL or root domain
        if scan_input[0].isalpha():
            url_scan(scan_input)
            ibm_url(scan_input)
            vt_url(scan_input)
            globalvar.MALREPORT.close()
            return 1
        # Scan IPv4 address 
        elif ipaddress.IPv4Network(scan_input):
            who_is(scan_input)
            ibm_ip(scan_input)
            vt_ip(scan_input)
            globalvar.MALREPORT.close()
            return 1
        # Error window
        else:
            return 0
    except ValueError:
        return 0
    except IndexError:
        return 0


# Stylesheet for GUI windows
stylesheet = """

QTextEdit {
    font-size: 12pt;  
}

QLabel {
    font-size: 11pt;
}

QLineEdit {
    font-size: 11pt;
}

QPushButton {
    font-size: 11pt;
}

QWidget {
    background-color: lightblue;
}

"""

if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setStyleSheet(stylesheet)
    mainWin = MainWindow()
    mainWin.show()
    sys.exit(app.exec_())
## AutomationOfOSINT
Automating the collection of open source intelligence (OSINT) from VirusTotal, IBM X-Force Exchange, urlscan.io and ipwhois.io.

## Description
This project uses Python and PyQt5 to create a tool that automates the collection of OSINT from the input of an IPv4 address, URL or root domain using public APIs. OSINT, such as IOC reputation and geolocation data, is collected from OSINT resources:
- VirusTotal (https://www.virustotal.com/gui/home/upload)
- IBM X-Force Exchange (https://exchange.xforce.ibmcloud.com/)
- urlscan.io (https://urlscan.io/) 
- ipwhois.io (https://ipwhois.io/)

## Installation
```bash
pip install requests
pip install ipaddress
pip install PyQt5
pip install requests-auth
pip install urllib3
pip install Pillow
```

## To run

1. Register free user acounts with VirusTotal, IBM X-Force Exchange and urlscan.io to retrieve your API keys and passwords. Please note ipwhois.io does not require an API key.
2. Enter these into the allocated places in the apikeys.json file, located in the 'source' directory, and save the file.
3. Open the terminal in the 'source' directory, or cd into the 'source' directory.
4. Type the below command into terminal to start the software:
```bash
python main.py
```
